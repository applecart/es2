﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace es2.Models
{
    public class Customer
    {
        [Required]
        [Display(Name = "Customer#")]
        public int CustID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Customer Name")]
        public string CustName { get; set; }


        [StringLength(50)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string CustEmail { get; set; }


        [Required]
        [Display(Name = "Plan Name")]
        public int PlanID { get; set; }
    }
}

/*
CREATE TABLE [dbo].[Customers](
	[CustID] [int] IDENTITY(1,1) NOT NULL,
	[CustName] [varchar](50) NULL,
	[CustEmail] [varchar](50) NULL,
	[PlanID] [int] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
 
 */
