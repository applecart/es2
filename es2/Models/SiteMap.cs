﻿namespace App
{
    public class SiteMap
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ControllerName { get; set; }
        public string ActionMethod { get; set; }
    }
}