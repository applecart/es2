﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace es2.Models
{
    public class Plan
    {
        public int PlanID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Plan Name")]
        public string PlanName { get; set; }

        [Required]
        [Display(Name = "Fixed Cost")]
        [DataType(DataType.Currency)]
        public decimal  PlanFixedCost { get; set; }

        [Required]
        [Display(Name = "Variable Cost")]
        [DataType(DataType.Currency)]
        public decimal PlanVarCost { get; set; }

    }
}

/*
 CREATE TABLE [dbo].[Plans](
	[PlanID] [int] IDENTITY(1,1) NOT NULL,
	[PlanName] [varchar](50) NULL,
	[PlanFixedCost] [decimal](8, 4) NULL,
	[PlanVarCost] [decimal](8, 4) NULL,
 CONSTRAINT [PK_Plans] PRIMARY KEY CLUSTERED 
(
	[PlanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
 */
