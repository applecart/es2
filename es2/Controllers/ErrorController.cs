﻿using System.Web.Mvc;

namespace es2.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult HttpError401()
        {
            return View();
        }

        public ActionResult HttpError404()
        {
            return View();
        }

        public ActionResult HttpError500()
        {
            return View();
        }

        public ActionResult General(string message)
        {
            return View();
        }
    }
}