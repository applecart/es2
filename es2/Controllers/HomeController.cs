﻿using es2.App_Classes;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace es2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           ViewData["Title"] = "Dashboard";
           ViewBag.PreviousAction = "Home";
           ViewBag.PreviousController = "Home";
           return View();
        }

        public ActionResult Home()
        {
            ViewData["Title"] = "Home";
            var oList = GetListFromSiteMap("Home.sitemap");
            return View(oList);
        }

        public ActionResult About()
        {
            ViewData["Title"] = "Administration";
            return View();
        }

        public ActionResult PrepList(DataTableAjaxPostModel model)
        {
            var repo = new ListCustomerPlusPlan();
            return Json(repo.LoadJqueryDataTable(model));

        }

        private System.Collections.Generic.List<App.SiteMap> GetListFromSiteMap(string siteMapName)
        {
            var retVal = new System.Collections.Generic.List<App.SiteMap>();
            var xdoc = System.Xml.Linq.XElement.Load(System.IO.Path.Combine(CommonCode.GetAppPath(), "SiteMaps", siteMapName));
            var lv1s = from lv1 in xdoc.Descendants()
                       select new
                       {
                           Children = lv1.Descendants()
                       };

            foreach (var lv1 in lv1s)
            {
                foreach (var node in lv1.Children)
                {
                    var url = node.Attribute("url").Value.Split('/');
                    retVal.Add(new App.SiteMap()
                    {
                        Title = node.Attribute("title").Value,
                        Description = node.Attribute("description").Value,
                        ImageUrl = VirtualPathUtility.ToAbsolute(node.Attribute("ImageUrl").Value),
                        ActionMethod = url.Last(),
                        ControllerName = url[url.Length -2]
                    });
                }
            }

            return retVal;

        }

    } // class
}