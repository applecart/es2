﻿using System;
using System.Web.Mvc;

namespace es2.Controllers
{
    public class PlansController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Title"] = "Home > Plans";
            ViewBag.PreviousAction = "Home";
            ViewBag.PreviousController = "Home";
            ViewBag.CrudAction = "Add";
            return View(GetPlans());
        }

        public ActionResult Add()
        {
            PrepFormControls();
            ViewData["Title"] = "Home > Plans > Adding a plan";
            ViewBag.CrudAction = "Create";
            var plan = new es2.Models.Plan();
            return View("Edit", plan);

        }

        public ActionResult Edit(int id)
        {
            PrepFormControls();
            ViewData["Title"] = "Home > Plans > Editing a plan";
            ViewBag.CrudAction = "Update";
            var plan = GetPlan(id) ; // find by primary key
            return View(plan);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(es2.Models.Plan plan)
        {
            ViewData["Title"] = "Home > Plans > Adding a plan";
            if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", plan); }

            AddPlan(plan);

            return RedirectToAction(GetIndexAction());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(es2.Models.Plan plan, string updateType)
        {
            ViewData["Title"] = "Home > Plans > Editing a plan";

            if (updateType.Equals("Delete"))
            {
                DeletePlan(plan);
            }
            else
            {
                if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", plan); }
                UpdatePlan(plan);
            }
            return RedirectToAction(GetIndexAction());

        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters;

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);
        }

        private System.Data.DataTable GetPlans()
        {
            var sql = @"
                SELECT
                  a.PlanID
                , a.PlanName
                , a.PlanFixedCost
                , a.PlanVarCost
                FROM
                   [Plans] a
            ";
            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                return rs.ExecuteQuery(sql);
            }
        }

        private es2.Models.Plan GetPlan(int planID)
        {
            var sql = @"
                SELECT
                  a.PlanID
                , a.PlanName
                , a.PlanFixedCost
                , a.PlanVarCost
                FROM
                   [Plans] a
                WHERE
                   a.PlanID = @PlanID 
            ";
            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                rs.SetParameterValue("PlanID", planID); 
                var row = rs.ExecuteQuery(sql).Rows[0];
                return new es2.Models.Plan
                {
                    PlanID = Convert.ToInt32(row["PlanID"]),
                    PlanName = row["PlanName"].ToString(),
                    PlanFixedCost = Convert.ToDecimal(row["PlanFixedCost"]),
                    PlanVarCost = Convert.ToDecimal(row["PlanVarCost"])
                };
            }
        }

        private void AddPlan(es2.Models.Plan plan)
        {
            var sql =
        @"

            INSERT INTO [Plans]
            (
     
                 [PlanName]
	            ,[PlanFixedCost]
	            ,[PlanVarCost]

            ) VALUES (
     
                 @PlanName
	            ,@PlanFixedCost
	            ,@PlanVarCost

            )

            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {

                rs.SetParameterValue("PlanName", plan.PlanName);
                rs.SetParameterValue("PlanFixedCost", plan.PlanFixedCost );
                rs.SetParameterValue("PlanVarCost", plan.PlanVarCost);

                rs.ExecuteNonQuery(sql);
            }

        }

        private void DeletePlan(es2.Models.Plan plan)
        {
        //    var sql =
        //@"

        //    DELETE FROM [Plans] WHERE PlanID = @PlanID 

        //    ";

        //    using (var rs = new Framework_Code.DataAccess("ES"))
        //    {

        //        rs.SetParameterValue("PlanID", plan.PlanID);

        //        rs.ExecuteNonQuery(sql);
        //    }

        }

        private void UpdatePlan(es2.Models.Plan plan)
        {
            // no code at this time

        }


    } // class

}