﻿using es2.App_Classes;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;

namespace es2.Controllers
{
    public class CustomersController : Controller
    {

        public ActionResult PrepList(DataTableAjaxPostModel model)
        {

            //var x = new WebApiWrapper<CustomersViewModel>(CommonCode.UrlCombine(CommonCode.GetAppUrl(), "api", "CustomersApi"));
            //var retVal = await x.JqueryDataTablePost(model);
            //return Json(retVal, JsonRequestBehavior.AllowGet);

            var innerJoin = _DbContext.Customers.Join(_DbContext.Plans
                    , pe => pe.PlanID, pl => pl.PlanID
                    , (pe, pl) => new CustomersViewModel
                    {
                        CustEmail = pe.CustEmail,
                        CustID = pe.CustID,
                        CustName = pe.CustName,
                        PlanName = pl.PlanName
                    });

            var repo = new ListCustomers();
            return Json(repo.LoadJqueryDataTable(model, innerJoin));
            
        }

        public ActionResult Index()
        {
            ViewData["Title"] = "Home > Customers";
            ViewBag.PreviousAction = "Home";
            ViewBag.PreviousController = "Home";
            ViewBag.CrudAction = "Add";
            return View();

        }

        public ActionResult Add()
        {
            PrepFormControls();
            ViewData["Title"] = "Home > Customers > Adding a customer";
            ViewBag.CrudAction = "Create";
            var plan = new es2.Models.Customer();
            return View("Edit", plan);

        }

        public async Task<ActionResult> Edit(int id)
        {
            PrepFormControls();
            ViewData["Title"] = "Home > Customers > Editing a customer";
            ViewBag.CrudAction = "Update";

            var customer = await GetCustomer(id); // find by primary key

            return View(customer);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(es2.Models.Customer customer, string updateType)
        {
            ViewData["Title"] = "Home > Customers > Editing a customer";

            if (updateType.Equals("Delete"))
            {
                DeleteCustomer(customer);
            }
            else
            {
                if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", customer); }
                UpdateCustomer(customer);
            }
            return RedirectToAction(GetIndexAction());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(es2.Models.Customer customer)
        {
            ViewData["Title"] = "Home > Customers > Adding a customer";
            if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", customer); }

            AddCustomer(customer);

            return RedirectToAction(GetIndexAction());

        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters;
            ViewBag.Plans = GetPlans();

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);

        }

        private class Plans
        {
            public int PlanID { get; set; }
            public string PlanName { get; set; }
        }

        private List<Plans> GetPlans()
        {
            var retVal = new List<Plans>();
            var sql = @"
                SELECT
                  b.PlanID
                , b.PlanName
                FROM
                   [Plans] b
            ";
            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                foreach (System.Data.DataRow row in rs.ExecuteQuery(sql).Rows)
                {
                    retVal.Add(new Plans { PlanID = Convert.ToInt32(row["PlanID"]), PlanName = row["PlanName"].ToString() });
                }
            }

            return retVal;
        }

        // callled from wrapper.RequestHeaderPreparing += AddHeader; 
        private void AddHeader(object sender, WebApiWrapper<es2.Models.Customer>.Args e)
        {
            var username = "abc";
            var password = "123";
            var encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username} : {password}"));
            e.Client .DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
        }

        private async Task<es2.Models.Customer> GetCustomer(int custID)
        {
            //var x = new WebApiWrapper<es2.Models.Customer>(CommonCode.UrlCombine(CommonCode.GetAppUrl(), "api", "CustomersApi"));
            //x.RequestHeaderPreparing += AddHeader; 
            //return await x.Get(custID.ToString());

            var sql = @"
                SELECT
                  a.CustID
                , a.CustName
                , a.CustEmail
                , a.PlanID
                FROM
                   [Customers] a
                WHERE
                   a.CustID = @CustID 
            ";
            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                rs.SetParameterValue("CustID", custID);
                var row = rs.ExecuteQuery(sql).Rows[0];
                return new es2.Models.Customer
                {
                    PlanID = Convert.ToInt32(row["PlanID"]),
                    CustName = row["CustName"].ToString(),
                    CustEmail = row["CustEmail"].ToString(),
                    CustID = Convert.ToInt32(row["CustID"])
                };
            }
        }

        private void AddCustomer(es2.Models.Customer customer)
        {
            var sql =
        @"

            INSERT INTO [Customers]
            (
     
                 [CustName]
	            ,[CustEmail]
	            ,[PlanID]

            ) VALUES (
     
                 @CustName
	            ,@CustEmail
	            ,@PlanID

            )

            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {

                rs.SetParameterValue("CustName", customer.CustName);
                rs.SetParameterValue("CustEmail", customer.CustEmail);
                rs.SetParameterValue("PlanID", customer.PlanID);

                rs.ExecuteNonQuery(sql);
            }

        }

        private void DeleteCustomer(es2.Models.Customer customer)
        {
            var sql =
        @"
                DELETE FROM [Customers] WHERE CustID = @CustID 
                ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                rs.SetParameterValue("CustID", customer.CustID);
                rs.ExecuteNonQuery(sql);
            }

        }

        private void UpdateCustomer(es2.Models.Customer customer)
        {
            var sql =
        @"
            UPDATE [Customers] SET
                 [CustName] = @CustName
	            ,[CustEmail] = @CustEmail
	            ,[PlanID] = @PlanID
            WHERE
                [CustID] = @CustID
            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {

                rs.SetParameterValue("CustName", customer.CustName);
                rs.SetParameterValue("CustEmail", customer.CustEmail);
                rs.SetParameterValue("PlanID", customer.PlanID);
                rs.SetParameterValue("CustID", customer.CustID);

                rs.ExecuteNonQuery(sql);

            }

        }

        private readonly ApplicationDbContext _DbContext;
        public CustomersController() { _DbContext = new ApplicationDbContext(); } // ctor
        protected override void Dispose(bool disposing) { _DbContext.Dispose(); }


    } // class
}