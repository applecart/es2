﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace es2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            // Register API routes
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            Server.ClearError();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action = "HttpError404";

                switch (httpException.GetHttpCode())
                {
                    case 401:
                        // unauthorized access
                        action = "HttpError401";
                        break;
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                }
                Response.Redirect(String.Format("~/Error/{0}", action));

            }
            else
            {
                if (exception.Message.Contains("(401)") || exception.InnerException.Message.Contains("401"))
                {
                    Response.Redirect(String.Format("~/Error/HttpError401"));
                }
                else
                {
                    Response.Redirect(String.Format("~/Error/General/?message={0}", exception.Message));
                }
            }

        }

    } // class
}
