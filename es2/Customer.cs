namespace es2
{
    using System.ComponentModel.DataAnnotations;

    public partial class Customer
    {
        [Key]
        public int CustID { get; set; }

        [StringLength(50)]
        public string CustName { get; set; } 

        [StringLength(50)]
        public string CustEmail { get; set; }

        public int? PlanID { get; set; }
    }

}
