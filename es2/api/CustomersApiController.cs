﻿using es2.App_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace es2.api
{

    public class CustomersApiController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Post(DataTableAjaxPostModel model)
        {
            var innerJoin = _DbContext.Customers.Join(_DbContext.Plans
                    , pe => pe.PlanID, pl => pl.PlanID 
                    , (pe, pl) => new CustomersViewModel{
                        CustEmail = pe.CustEmail,
                        CustID = pe.CustID,
                        CustName = pe.CustName,  
                        PlanName = pl.PlanName  
                    });

            var repo = new ListCustomers();

            return Ok(repo.LoadJqueryDataTable(model, innerJoin) );

        }

        [Restrict(Roles="Role 2")]
        public IHttpActionResult Get(int id)
        {
            var customers = _DbContext.Customers;
            var customer = customers.Find(id); // find by primary key

            return Ok(customer);

        }

        private readonly ApplicationDbContext _DbContext;
        public CustomersApiController() { _DbContext = new ApplicationDbContext(); } // ctor
        protected override void Dispose(bool disposing) { _DbContext.Dispose(); }

    }
}
