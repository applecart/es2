﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace es2.App_Classes
{

    public class CustomerPlusPlanViewModel
    {
        public int CustID { get; set; }
        public string CustName { get; set; }
        public string CustEmail { get; set; }
        public string PlanName { get; set; }
        public decimal PlanFixedCost { get; set; }
        public decimal PlanVarCost { get; set; }
    }

    public class ListCustomerPlusPlan : AbstractClasses.ListEntityBase<CustomerPlusPlanViewModel>
    {
        protected override IEnumerable<CustomerPlusPlanViewModel> GetData(string whereClause, string exactFilter, string likeFilter, string orderByClause, string fetchAndOffsetClause)
        {
            var sql = @"
                SELECT
                  a.CustID
                , a.CustName
                , a.CustEmail
                , b.PlanName
                , b.PlanFixedCost
                , b.PlanVarCost
                FROM
                  [Customers] a
                INNER JOIN
                   [Plans] b
                ON
                   a.PlanID = b.PlanID
                {0} {1} {2}
            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                rs.SetParameterValue("ExactFilter", exactFilter);
                rs.SetParameterValue("LikeFilter", likeFilter);
                foreach(System.Data.DataRow row in rs.ExecuteQuery(string.Format(sql, whereClause,orderByClause, fetchAndOffsetClause)).Rows)
                {
                    yield return 
                        new CustomerPlusPlanViewModel() {
                        CustID = Convert.ToInt32(row["CustID"]),
                        PlanFixedCost = Convert.ToDecimal(row["PlanFixedCost"]),
                        PlanVarCost = Convert.ToDecimal(row["PlanVarCost"]),
                        CustEmail = row["CustEmail"].ToString(),
                        CustName = row["CustName"].ToString(),
                        PlanName = row["PlanName"].ToString()
                    };  
                }
            }

        }

        protected override int GetFilteredResultsCount(string whereClause, string exactFilter, string likeFilter)
        {
            var sql = @"
                SELECT
                    COUNT(1) 
                FROM
                  [Customers] a
                INNER JOIN
                   [Plans] b
                ON
                   a.PlanID = b.PlanID
                {0}
            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                rs.SetParameterValue("ExactFilter", exactFilter);
                rs.SetParameterValue("LikeFilter", likeFilter);
                return Convert.ToInt32(rs.ExecuteScalar(string.Format(sql,whereClause)));
            }

        }

        protected override int GetTotalResultsCount()
        {
            var sql = @"
                SELECT
                    COUNT(1) 
                FROM
                  [Customers] a
                INNER JOIN
                   [Plans] b
                ON
                   a.PlanID = b.PlanID
            ";

            using (var rs = new Framework_Code.DataAccess("ES"))
            {
                return Convert.ToInt32(rs.ExecuteScalar(sql));
            }

        }

        protected override string GetWhereClause()
        {
            return @"
                CAST(CustID AS VARCHAR(11)) = @ExactFilter
                OR
                CustName LIKE @LikeFilter
                OR
                CustEmail LIKE @LikeFilter
                OR
                PlanName LIKE @LikeFilter
                OR
                CAST(PlanFixedCost AS VARCHAR(11)) = @ExactFilter
                OR
                CAST(PlanVarCost AS VARCHAR(11)) = @ExactFilter
            ";
        }
    }
}