﻿
/*
Class abstracts Web Api interfacing chores using HttpClient

Requirements:
    1) .net 4.5 or better
    2) nuget package Microsoft.AspNet.WebApi.Client
    3) Add Reference to System.Net.Http
*/

using System.Threading.Tasks;
using System.Net.Http;

public class WebApiWrapper<InputModel>
{
    private string _Url;
    private System.Net.Http.HttpClient _Client;

    public WebApiWrapper(string url)
    {
        _Url = url;
        // _Client should not be used in a using block even thought it implements IDisposable
        // https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
        _Client = new System.Net.Http.HttpClient();

    }

    public async Task<InputModel> Get(string id)
    {
        PrepRequestHeader();
        var data = await _Client.GetStringAsync(GetUrl(id));
        return Newtonsoft.Json.JsonConvert.DeserializeObject<InputModel>(data);

    }

    public async Task Post(InputModel fromBody)
    {
        PrepRequestHeader();
        var content = GetContent(fromBody);
        var dummy = await _Client.PostAsync(GetUrl(), content);

    }

    public async Task<OutputModel> Post<OutputModel, NewInputModel>(NewInputModel fromBody)
    {
        PrepRequestHeader();
        var content = GetContent<NewInputModel>(fromBody);
        var result = await _Client.PostAsync(GetUrl(), content);
        return await Extract<OutputModel>(result);

    }

    public async Task<OutputModel> Post<OutputModel>(InputModel fromBody)
    {
        return await Post<OutputModel, InputModel>(fromBody);

    }

    public async Task<AjaxResponseModel> JqueryDataTablePost(DataTableAjaxPostModel dataTableAjaxPostModel)
    { // this Post is used in conjunction with jQuery data table serverside processing
      // server side processing is initiated by jQuery ajax calls from views
        return await Post<AjaxResponseModel, DataTableAjaxPostModel>(dataTableAjaxPostModel);

    }

    public class AjaxResponseModel
    { // this Post is used in conjunction with jQuery data table serverside processing
        public int draw, recordsTotal, recordsFiltered;
        public System.Collections.Generic.IEnumerable<InputModel> data;

    }

    public async Task<T> Extract<T>(System.Net.Http.HttpResponseMessage response)
    { // extracts from the response the object of type T
        return await response.Content.ReadAsAsync<T>();

    }

    public async Task Put(InputModel fromBody)
    {
        PrepRequestHeader();
        var content = GetContent(fromBody);
        var dummy = await _Client.PutAsync(GetUrl(), content);

    }

    public async Task Delete(string id)
    {
        PrepRequestHeader();
        var dummy = await _Client.DeleteAsync(GetUrl(id));

    }

    private System.Net.Http.StringContent GetContent<T>(T fromBody)
    {
        return new System.Net.Http.StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(fromBody), System.Text.Encoding.UTF8, "application/json");
    }

    private void PrepRequestHeader()
    {
        _Client.DefaultRequestHeaders.Clear();
        _Client.DefaultRequestHeaders
          .Accept
          .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        OnRequestHeaderPreparing(_Client);

        /*
        // callled from wrapper.RequestHeaderPreparing += AddHeader;
        private void AddHeader(object sender, WebApiWrapper<es2.Models.Customer>.Args e)
        {
            var username = "abc";
            var password = "123";
            var encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username} : {password}"));
            e.Client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
        }
        */
    }

    private string GetUrl()
    {
        return _Url;

    }

    private string GetUrl(string id)
    {
        return string.Format(@"{0}/{1}", _Url, id);

    }

    #region "Observer pattern"

    public class Args : System.EventArgs
    {
        public System.Net.Http.HttpClient Client { get; set; }
    }

    public event System.EventHandler<Args> RequestHeaderPreparing;

    protected virtual void OnRequestHeaderPreparing(System.Net.Http.HttpClient client)
    {
        if (HasSubscriber(RequestHeaderPreparing))
        {
            RequestHeaderPreparing(this, new Args() { Client = client });
        }

    }

    private bool HasSubscriber(System.EventHandler<Args> anEvent)
    {
        return anEvent != null;
    }


    #endregion

}


