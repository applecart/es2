﻿/*
 
    A helper class for bulk copy operations on Sql Server with transaction.
    This class inherits from TxnWrite.
 
    Methods:
            CreateColumnMapFile - Creates a column map file aligning source columns with destination columns for use with ExecuteBulkCopy()
            ExecuteBulkCopy - Bulk copies data provided either as a data table or as a data reader object

    Sample usage in the code:
    -------------------------

    protected void Button1_Click(object sender, EventArgs e)
    {

        System.Data.DataTable dt;

        using (var rs = new Framework_Code.DataAccess("201A"))
        {
            dt = rs.ExecuteQuery("SELECT * FROM [dbo].[SAPPaymentsUpload]");
        }

        using (var rs = new Framework_Code.TxnWriteSqlServer("150"))
        {
            try
            {
                rs.ExecuteBulkCopy("[dbo].[SAPPaymentsUpload]", dt, new System.Collections.Generic.Dictionary<string, string>());
                throw new System.Exception("Will cause rollback"); // Comment this line to commit successfully
                rs.Commit();
            }
            catch
            {
                rs.Rollback();
            }

            this.Literal1.Text = rs.IsCommitted.ToString();

        }
    }
 
    Denny Jacob -  03/05/2017
    Last modified on - 03/28/2017

*/

namespace Framework_Code
{

    public class TxnWriteSqlServer : TxnWrite
    {
        public TxnWriteSqlServer(string provider, string connectionLiteral) : base(provider, connectionLiteral) { }
        public TxnWriteSqlServer(string connectionName) : base(connectionName) { }

        public void ExecuteBulkCopy(
                  string dboTableName
                , System.Data.DataTable dataTable
                , System.Collections.Generic.Dictionary<string, string> columnsMap)
        {
            ExecuteBulkCopy(dboTableName, dataTable.CreateDataReader(), columnsMap);
        }

        public void ExecuteBulkCopy(
                  string dboTableName
                , System.Data.Common.DbDataReader oDbDataReader
                , System.Collections.Generic.Dictionary<string, string> columnsMap)
        {
            using (var bulkCopy = new System.Data.SqlClient.SqlBulkCopy(
                        (System.Data.SqlClient.SqlConnection)m_DbConn
                       , System.Data.SqlClient.SqlBulkCopyOptions.KeepIdentity | System.Data.SqlClient.SqlBulkCopyOptions.KeepNulls
                       , (System.Data.SqlClient.SqlTransaction)m_DbCmd.Transaction
                       )
           )
            {
                bulkCopy.DestinationTableName = dboTableName.Trim();
                bulkCopy.BulkCopyTimeout = 0; // no timeout
                bulkCopy.BatchSize = 3000;
                foreach (var key in columnsMap.Keys)
                {
                    bulkCopy.ColumnMappings.Add(columnsMap[key], key);
                }
                bulkCopy.WriteToServer(oDbDataReader);
            }

        }

        /// <summary>
        /// Creates a column map file aligning source columns with destination columns as name/value pairs
        /// </summary>
        /// <param name="mapFilename">File to prepare the source target column map info</param>
        /// <param name="dataTable">data table containing the (source) data to populate</param>
        /// <remarks></remarks>
        public void CreateColumnMapFile(string mapFilename, System.Data.DataTable dataTable)
        {
            var lines = new System.Collections.Generic.List<string>();

            lines.Add("private System.Collections.Generic.Dictionary<string, string> GetColumnMap(){");
            lines.Add("var columnsMap = new System.Collections.Generic.Dictionary<string, string>();");
            lines.Add("// The destination columns are on the left so source columns may be re-used.");

            foreach (System.Data.DataColumn column in dataTable.Columns)
            {
                lines.Add(string.Format("columnsMap.Add(\"[{0}]\",\"{0}\");", column.ColumnName));
            }

            lines.Add(string.Empty);
            lines.Add("return columnsMap;");
            lines.Add(string.Empty);
            lines.Add("}");

            System.IO.File.WriteAllLines(mapFilename, lines);

        }

    } // class TxnWriteSqlServer

}

