﻿/*
A wrapper on jQuery datatable's serverside implementation

Sample script on the View
-------------------------
    <script type="text/javascript" src="~/Scripts/DataTables/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#listItems').DataTable({
                //lengthMenu: [[100, 50], [100, 50]],
                proccessing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: '@Url.Action("PrepList", "Customers")',
                    data: { appendix: 'additional string param'},
                    type: 'POST'
                },
               columns: [
                   { data: 'EditLink', orderable: false },
                   { data: 'CustID', type: 'num' , className: 'dt-right', render: renderLink, width: '11%' }
                   { data: 'CustName' },
                   { data: 'CustEmail' },
                   { data: 'PlanName' }
                ],
                order: [[1, 'desc']] // default sort on col #2 
            });
        });

        function renderLink(data, type, row, meta) {
            // backticks are needed for string interpolation in JavaScript
            return `<a href="@Url.Action(
                            "Index",
                            "DownloadedPatients")?id=${row.ID}"
            class="btn btn-link"
            title="View patient list in this batch">
            ${row.Total}
            <span class="glyphicon glyphicon-zoom-in" style="color:salmon;" aria-hidden="true"></span>
            </a>`

        }

    </script>

Sample table body on the View
------------------------------
<table class="table table-striped" id="listItems">
    <thead>
        <tr>
            <th>
            </th>
            <th>
                Customer#
            </th>
            <th>
                Customer Name
            </th>
            <th>
                Email
            </th>
            <th>
                Plan Name
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5">Loading data...</td>
        </tr>
    </tbody>
</table>

Sample code in the Controller
-----------------------------
        public ActionResult PrepList(DataTableAjaxPostModel model)
        {

            var innerJoin = _DbContext.Customers.Join(_DbContext.Plans
                    , pe => pe.PlanID, pl => pl.PlanID
                    , (pe, pl) => new CustomersViewModel
                    {
                        CustEmail = pe.CustEmail,
                        CustID = pe.CustID,
                        CustName = pe.CustName,
                        PlanName = pl.PlanName
                    });

            var repo = new ListCustomers();
            return Json(repo.LoadJqueryDataTable(model, innerJoin));
            
        }

Class definition for DataTableAjaxPostModel
--------------------------------------------

using System.Collections.Generic;

public class DataTableAjaxPostModel
{
    // properties are not capitalized to facilitate json mapping
    public int draw { get; set; }
    public int start { get; set; }
    public int length { get; set; }
    public List<Column> columns { get; set; }
    public Search search { get; set; }
    public List<Order> order { get; set; }
    public string appendix { get; set; }
}

public class Column
{
    public string data { get; set; }
    public string name { get; set; }
    public bool searchable { get; set; }
    public bool orderable { get; set; }
    public Search search { get; set; }
}

public class Search
{
    public string value { get; set; }
    public string regex { get; set; }
}

public class Order
{
    public int column { get; set; }
    public string dir { get; set; }
}

*/

using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace AbstractClasses
{
    public abstract class ListEntityEfBase<ViewModel>
    {
        public class Result
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public IEnumerable<ViewModel> data { get; set; }

        }

        protected string Appendix { get; set; }
        protected bool IsExactFilterOnly = false;

        private string _Filter;
        protected string Filter
        {
            get { return _Filter; }

        }

        protected Search Search
        {
            set
            {
                _Filter = value.value;

                if (_Filter != null)
                {
                    var quotationMark = $"{(char)34}";

                    IsExactFilterOnly =
                        !_Filter.Equals(quotationMark) &&
                        _Filter.StartsWith(quotationMark) &&
                        _Filter.EndsWith(quotationMark);

                    if (IsExactFilterOnly) _Filter = _Filter.Trim('"');

                }

            }

        }

        // this is the only public method in this class
        public Result LoadJqueryDataTable(DataTableAjaxPostModel model, IQueryable<ViewModel> entity)
        {
            Appendix = model.appendix;
            Search = model.search; 

            return new Result
            {
                data = GetData(GetResults(model, entity)).ToList(),
                draw = model.draw,
                recordsTotal = entity.Count(),
                recordsFiltered = GetFilteredResults(model, entity).Count(),

            };

        }

        // Try and use "yield return" in the following method so the output is streamed
        protected abstract IEnumerable<ViewModel> GetData(IQueryable<ViewModel> entity);

        protected abstract IQueryable<ViewModel> GetFilteredResults(DataTableAjaxPostModel model, IQueryable<ViewModel> entity);

        protected IQueryable<ViewModel> GetResults(DataTableAjaxPostModel model, IQueryable<ViewModel> entity)
        {
            var sortOrder = new List<string>();
            foreach (var orderBy in model.order)
            {
                sortOrder.Add($"{model.columns[orderBy.column].data} {orderBy.dir}");

            }

            if (sortOrder.Count() == 0) sortOrder.Add($"{model.columns[1].data} DESC");

            return GetFilteredResults(model, entity)
                .OrderBy(string.Join(",", sortOrder)) // this OrderBy() is from System.Linq.Dynamic which is a nuget package 
                .Skip(model.start)
                .Take(model.length);

        }

        protected virtual string GetEditLink(string actionMethod, string controllerName, string id)
        {
            return $"<a Class=\"btn btn-link glyphicon glyphicon-pencil\" href=\"/{controllerName}/{actionMethod}/{id}\" title=\"Edit this record\"> Edit</a>";

        }

    } // class

}

/*
 
-------------------------------------------------------------------
Concrete class definition of ListCustomers DO NOT COPY BUT COMPOSE
-------------------------------------------------------------------
using System.Collections.Generic;
using System.Linq;

namespace es2.App_Classes
{

    public class CustomersViewModel
    {
        public string EditLink { get; set; }
        public int CustID { get; set; }
        public string CustName { get; set; }
        public string CustEmail { get; set; }
        public string PlanName { get; set; }
    }

    public class ListCustomers : AbstractClasses.ListEntityEfBase<CustomersViewModel>
    {
        protected override IEnumerable<CustomersViewModel> GetData(IQueryable<CustomersViewModel> entity)
        {
            foreach (var row in entity.ToList())
            {
                yield return new CustomersViewModel
                {
                    CustEmail = row.CustEmail,
                    CustID = row.CustID,
                    CustName = row.CustName,
                    PlanName = row.PlanName,
                    EditLink = GetEditLink("Edit", "Customers", row.CustID.ToString())

                };

            }
        }

        protected override IQueryable<CustomersViewModel> GetFilteredResults(DataTableAjaxPostModel model, IQueryable<CustomersViewModel> entity)
        {
            if (Filter == null)
            {
                return entity;

            }
            else
            {
                return entity.Where(e =>
                    e.CustID.ToString().Equals(Filter)
                    ||
                    (this.IsExactFilterOnly ? e.CustName.ToString().Equals(Filter) : e.CustName.ToString().Contains(Filter))
                    ||
                    (this.IsExactFilterOnly ? e.PlanName.ToString().Equals(Filter) : e.PlanName.ToString().Contains(Filter))
                    ||
                    (this.IsExactFilterOnly ? e.CustEmail.ToString().Equals(Filter) : e.CustEmail.ToString().Contains(Filter))
                    );

            }

        }

    } // class

}

*/
