﻿/*
A wrapper on jQuery datatable's serverside implementation

Sample script on the View
-------------------------
    <script type="text/javascript" src="~/Scripts/DataTables/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#listItems').DataTable({
                //lengthMenu: [[100, 50], [100, 50]],
                proccessing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: '@Url.Action("PrepList", "Customers")',
                    data: { appendix: 'additional string param'},
                    type: 'POST'
                },
               columns: [
                   { data: 'EditLink', orderable: false },
                   { data: 'CustID', type: 'num' , className: 'dt-right', render: renderLink, width: '11%' }
                   { data: 'CustName' },
                   { data: 'CustEmail' },
                   { data: 'PlanName' }
                ],
                order: [[1, 'desc']] // default sort on col #2 
            });
        });

        function renderLink(data, type, row, meta) {
            // backticks are needed for string interpolation in JavaScript
            return `<a href="@Url.Action(
                            "Index",
                            "DownloadedPatients")?id=${row.ID}"
            class="btn btn-link"
            title="View patient list in this batch">
            ${row.Total}
            <span class="glyphicon glyphicon-zoom-in" style="color:salmon;" aria-hidden="true"></span>
            </a>`

        }

    </script>

Sample table body on the View
------------------------------
<table class="table table-striped" id="listItems">
    <thead>
        <tr>
            <th>
            </th>
            <th>
                Customer#
            </th>
            <th>
                Customer Name
            </th>
            <th>
                Email
            </th>
            <th>
                Plan Name
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5">Loading data...</td>
        </tr>
    </tbody>
</table>

Sample code in the Controller
-----------------------------
    public ActionResult PrepList(DataTableAjaxPostModel model)
    {
		var repo = new SandboxRepository(); // di injected
		return Json(repo.LoadJqueryDataTable(model));

    }


Class definition for DataTableAjaxPostModel
--------------------------------------------

using System.Collections.Generic;

public class DataTableAjaxPostModel
{
    // properties are not capitalized to facilitate json mapping
    public int draw { get; set; }
    public int start { get; set; }
    public int length { get; set; }
    public List<Column> columns { get; set; }
    public Search search { get; set; }
    public List<Order> order { get; set; }
    public string appendix { get; set; }
}

public class Column
{
    public string data { get; set; }
    public string name { get; set; }
    public bool searchable { get; set; }
    public bool orderable { get; set; }
    public Search search { get; set; }
}

public class Search
{
    public string value { get; set; }
    public string regex { get; set; }
}

public class Order
{
    public int column { get; set; }
    public string dir { get; set; }
}

*/
using System;
using System.Collections.Generic;

namespace AbstractClasses
{

    public abstract class ListEntityBase<ViewModel>
    {
        public class Result
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public IEnumerable<ViewModel> data { get; set; }

        }

        protected string Appendix { get; set; }

        // this is the only public method in this class
        public Result LoadJqueryDataTable(DataTableAjaxPostModel model)
        {
            Appendix = model.appendix;

            _MyParameters = new MyParameters();

            _MyParameters.OrderByClause = GetOrderByClause(model);
            _MyParameters.OrderByClause = Environment.NewLine + (string.IsNullOrWhiteSpace(_MyParameters.OrderByClause) ? "ORDER BY 1" : "ORDER BY " + _MyParameters.OrderByClause);

            _MyParameters.Search = model.search;

            _MyParameters.WhereClause = GetWhereClause();
            _MyParameters.WhereClause = Environment.NewLine + (string.IsNullOrWhiteSpace(_MyParameters.WhereClause) ? _MyParameters.WhereClause : "WHERE" + _MyParameters.WhereClause);

            _MyParameters.FetchAndOffsetClause = $"{Environment.NewLine}OFFSET {model.start} ROWS FETCH NEXT {model.length} ROWS ONLY"; // skip and take

            return new Result
            {
                data = GetData(_MyParameters.WhereClause, _MyParameters.ExactFilter, _MyParameters.LikeFilter, _MyParameters.OrderByClause, _MyParameters.FetchAndOffsetClause),
                draw = model.draw,
                recordsTotal = GetTotalResultsCount(),
                recordsFiltered = GetFilteredResultsCount(_MyParameters.WhereClause, _MyParameters.ExactFilter, _MyParameters.LikeFilter),

            };

        }

        protected abstract string GetWhereClause();
        protected abstract int GetTotalResultsCount();
        protected abstract int GetFilteredResultsCount(string whereClause, string exactFilter, string likeFilter);
        // Try and use "yield return" in the following method so the output is streamed
        protected abstract IEnumerable<ViewModel> GetData(string whereClause, string exactFilter, string likeFilter, string orderByClause, string fetchAndOffsetClause);
        protected virtual string GetEditLink(string actionMethod, string controllerName, string id)
        {
            return $"<a Class=\"btn btn-link glyphicon glyphicon-pencil\" href=\"/{controllerName}/{actionMethod}/{id}\" title=\"Edit this record\"> Edit</a>";

        }

        private MyParameters _MyParameters;
        private class MyParameters
        {
            public string WhereClause { get; set; }
            public string OrderByClause { get; set; }
            public string FetchAndOffsetClause { get; set; }

            public string LikeFilter
            {
                get { return IsExactFilterOnly ? _ExactFilter : $"%{_ExactFilter}%"; }

            }

            private string _ExactFilter;
            public string ExactFilter
            {
                get { return _ExactFilter; }

            }

            public Search Search
            {
                set
                {
                    var exactFilter = value.value;
                    if (exactFilter != null)
                    {
                        var quotationMark = $"{(char)34}";

                        IsExactFilterOnly =
                            !exactFilter.Equals(quotationMark) &&
                            exactFilter.StartsWith(quotationMark) &&
                            exactFilter.EndsWith(quotationMark);

                        if (IsExactFilterOnly) exactFilter = exactFilter.Trim('"');

                    }

                    _ExactFilter = exactFilter;

                }

            }

            private bool IsExactFilterOnly = false;

        }

        private string GetOrderByClause(DataTableAjaxPostModel model)
        {
            var sortOrder = new List<string>();
            foreach (var orderBy in model.order)
            {
                sortOrder.Add($"{model.columns[orderBy.column].data} {orderBy.dir}");

            }

            return string.Join(",", sortOrder);

        }

    } // class

}

/*
For an example of the concrete implementation of this class, please refert to ListCustomerPlusPlan.cs in Portfolio with EF and WebAPI\App_Classes
*/
