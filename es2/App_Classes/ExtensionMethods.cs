﻿

public static class ExtensionMethods
{
    // Adds a new column to the data table with a default value
    public static void AddColumn(this System.Data.DataTable dataTable, string newColumnName, string defaultValue)
    {
        var col = new System.Data.DataColumn(newColumnName, typeof(string));
        col.DefaultValue = defaultValue;
        dataTable.Columns.Add(col);

    }

}
