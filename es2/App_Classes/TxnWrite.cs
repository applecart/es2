﻿
/*
 
    A helper class for transaction enabled data write.
    Methods:
            SetCommandTimeout - sets connection timeout in seconds, Default is 30 seconds.
            SetParameterValue - method supplies values to parameters in a sql query 
            ExecuteNonQuery - for queries that contain DML. Return the number of rows affected e.g. INSERT ...
            ExecuteScalar - for queries that return one result e.g. SELECT COUNT(*) ...
            Commit() - Writes everything in the transaction scope to the underlying tables.
            Rollback() - Prevents everything in the transaction scope from being written to the underlying tables.
            IsCommitted- Property indicates save status:
                True - indicates a successful commit operation.
                False - indicates transaction roll back or uncommitted transactions.

    Sample usage in the code:
    -------------------------
    protected void Button1_Click(object sender, System.EventArgs e)
    {
        string[] shipperNames = {
	         "First Shipping Company"
	        ,"Second Shipping Company"
	        ,"Third Shipping Company"
	        ,null
        };

        using (var rs = new Framework_Code.TxnWrite("NorthwindConnectionString"))
        {
            try
            {
                // Change this to 2 for a successful commit.
                for (int i = 0; i <= 3; i++)
                {
                    rs.SetParameterValue("CompanyName", shipperNames[i]);
                    rs.ExecuteNonQuery("INSERT INTO Shippers (CompanyName) VALUES (@CompanyName)");
                }
                rs.Commit();
            }
            catch 
            {
                rs.Rollback();
                // normally you'd re-throw the exception
                //throw;
            }
            this.Literal1.Text = rs.IsCommitted ? "Committed" : "Rolledback";
        }
    }


*/

namespace Framework_Code
{

    public class TxnWrite : System.IDisposable
    {

        protected System.Data.Common.DbConnection m_DbConn;
        protected System.Data.Common.DbCommand m_DbCmd;

        private bool m_IsCommitted;
        public bool IsCommitted
        {
            get { return m_IsCommitted; }

        }

        public TxnWrite(string provider, string connectionLiteral)
        {
            Dal(provider, connectionLiteral);

        }

        public TxnWrite(string connectionName)
        {
            Dal(System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ProviderName
              , System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);

        }

        public void SetCommandTimeout(int seconds)
        {
            m_DbCmd.CommandTimeout = seconds;

        }

        public void SetParameterValue(string parameterName, object parameterValue)
        {
            var dbParams = m_DbCmd.CreateParameter();
            dbParams.ParameterName = parameterName;
            if (parameterValue == null)
            {
                dbParams.Value = System.DBNull.Value;
            }
            else
            {
                dbParams.Value = parameterValue;
            }
            m_DbCmd.Parameters.Add(dbParams);

        }

        public void Commit()
        {
            // Always use this method in the Try scope of the Try-Catch that packs transaction enabled operations
            m_DbCmd.Transaction.Commit();
            m_IsCommitted = true;
            Dispose(false);

        }

        public void Rollback()
        {
            // Always use this method in the Catch scope of the Try-Catch that packs transaction enabled operations
            m_DbCmd.Transaction.Rollback();
            Dispose(false);

        }

        public int ExecuteNonQuery(string sql)
        {
            int retVal = 0;
            m_DbCmd.CommandText = sql;
            retVal = m_DbCmd.ExecuteNonQuery();
            m_DbCmd.Parameters.Clear();

            return retVal;

        }

        /// <summary>
        /// Use this method to obtain id of the newly added record by appending SELECT SCOPE_IDENTITY() to an INSERT sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>Autoincremented primary key value of the newly inserted record.</returns>
        /// <remarks>For MS Access execute scalar query SELECT @@IDENTITY subsequent to the INSERT query.</remarks>
        public object ExecuteScalar(string sql)
        {
            object retVal = null;
            m_DbCmd.CommandText = sql;
            retVal = m_DbCmd.ExecuteScalar();
            m_DbCmd.Parameters.Clear();

            return retVal;

        }

        // Called from the constructor
        private void Dal(string provider, string connectionLiteral)
        {
            m_DbConn = System.Data.Common.DbProviderFactories.GetFactory(provider).CreateConnection();
            m_DbConn.ConnectionString = connectionLiteral;
            m_DbCmd = m_DbConn.CreateCommand();
            m_DbCmd.Connection.Open();
            m_DbCmd.Transaction = m_DbConn.BeginTransaction();

        }

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                // free unmanaged resources (unmanaged objects) and override Finalize() below.
                m_DbCmd.Dispose();
                m_DbConn.Dispose();
            }
            this.disposedValue = true;

        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(true);
            System.GC.SuppressFinalize(this);

        }

        #endregion

    }

}

