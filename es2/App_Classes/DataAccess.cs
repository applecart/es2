﻿
/*

    A helper class for data access. Always instantiate this class on a "Using" block.
    Methods:
        SetParameterValue - method supplies values to parameters in a sql query 
        ExecuteNonQuery - for queries that contain DML. Return the number of rows affected e.g. INSERT ...
        ExecuteScalar - for queries that return one result e.g. SELECT COUNT(*) ...
        ExecuteQuery - for queries that return a DataTable
        ExecuteReaderMethod - executes a method which in turn uses data reader object from the query
        SetCommandTimeout - sets connection timeout in seconds, Default is 30 seconds.

    Denny Jacob - 11/05/2011
    Last modified on - 11/22/2016 

*/

namespace Framework_Code
{
    public class DataAccess : System.IDisposable
    {

        public delegate void addressOfMethodEventHandler(string key, System.Data.Common.DbDataReader oDbDataReader);
        // Delegate method for addressOfMethodEvent. Provide param signatures as needed.

        private System.Data.Common.DbConnection m_DbConn;
        private System.Data.Common.DbCommand m_DbCmd;

        public DataAccess(string provider, string connectionLiteral)
        {
            Dal(provider, connectionLiteral);

        }

        public DataAccess(string connectionName)
        {
            Dal(System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ProviderName
              , System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);

        }

        public void SetCommandTimeout(int seconds)
        {
            m_DbCmd.CommandTimeout = seconds;

        }

        public void SetParameterValue(string parameterName, object parameterValue)
        {
            var dbParams = m_DbCmd.CreateParameter();
            dbParams.ParameterName = parameterName;
            if (parameterValue == null)
            {
                dbParams.Value = System.DBNull.Value;
            }
            else
            {
                dbParams.Value = parameterValue;
            }
            m_DbCmd.Parameters.Add(dbParams);

        }

        public int ExecuteNonQuery(string sql)
        {
            try
            {
                m_DbCmd.CommandText = sql;
                m_DbCmd.Connection.Open();
                return m_DbCmd.ExecuteNonQuery();

            }
            finally
            {
                m_DbCmd.Parameters.Clear();
                m_DbCmd.Connection.Close();

            }

        }

        public object ExecuteScalar(string sql)
        {
            try
            {
                m_DbCmd.CommandText = sql;
                m_DbCmd.Connection.Open();
                return m_DbCmd.ExecuteScalar();

            }
            finally
            {
                m_DbCmd.Parameters.Clear();
                m_DbCmd.Connection.Close();

            }

        }

        // Classic implementation
        public System.Data.DataTable ExecuteSql(string sql)
        {
            return ExecuteQuery(sql);

        }

        public System.Data.DataTable ExecuteQuery(string sql)
        {
            try
            {
                m_DbCmd.CommandText = sql;
                m_DbCmd.Connection.Open();
                using (var retVal = new System.Data.DataTable())
                {
                    using (var oDbDataReader = m_DbCmd.ExecuteReader())
                    {
                        retVal.Load(oDbDataReader);
                        return retVal;

                    }
                }

            }
            finally
            {
                m_DbCmd.Parameters.Clear();
                m_DbCmd.Connection.Close();

            }

        }

        public void ExecuteReaderMethod(string sql, string key, addressOfMethodEventHandler addressOfMethod)
        {
            try
            {
                m_DbCmd.CommandText = sql;
                m_DbCmd.Connection.Open();
                using (var oDbDataReader = m_DbCmd.ExecuteReader())
                {
                    addressOfMethod.Invoke(key, oDbDataReader);
                }

            }
            finally
            {
                m_DbCmd.Parameters.Clear();
                m_DbCmd.Connection.Close();

            }

        }

        private void Dal(string provider, string connectionLiteral)
        {
            m_DbConn = System.Data.Common.DbProviderFactories.GetFactory(provider).CreateConnection();
            m_DbConn.ConnectionString = connectionLiteral;
            m_DbCmd = m_DbConn.CreateCommand();

        }

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                // free unmanaged resources (unmanaged objects) and override Finalize() below.
                m_DbCmd.Dispose();
                m_DbConn.Dispose();
            }
            this.disposedValue = true;

        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(true);
            System.GC.SuppressFinalize(this);

        }

        #endregion

    }

}

