﻿/*

    A serializable class for ad hoc reporting.
    It creates a datatable on-the-fly from rows supplied to it:
        via Data Table param of its New() or
        by using SetColumnValue() method to set one column at a time, and then appending the row using AddRow() method. 
    Output methods:
        ToDataView() 
        ToDataTable() 
        ToHtmlTable()
        ToCsv() from its non serializable subclass AdhocDataExtension
    You may test whether or not this object has rows by querying its HasRows() method or the count of rows from RowsCount()

    Denny Jacob - 04/02/2008
    Last modified on - 03/29/2013

*/

namespace Framework_Code
{

    [System.Serializable()]
    public class AdhocData : System.IDisposable
    {

        private System.Collections.Specialized.NameValueCollection m_NewDataRow
                    = new System.Collections.Specialized.NameValueCollection();
        private System.Data.DataTable m_DataTable = null; 

        public AdhocData()
        {
            m_DataTable = new System.Data.DataTable();

        }

        public AdhocData(System.Data.DataTable dataTable)
        {
            m_DataTable = dataTable;

        }

        public int RowsCount
        {
            get { return m_DataTable.Rows.Count; }

        }

        public bool HasRows
        {
            get { return RowsCount > 0; }

        }

        public void SetColumnValue(string columnName, string columnValue)
        {
            m_NewDataRow.Add(columnName, columnValue);

        }

        public void AddRow()
        {
            var col = m_DataTable.Columns;
            foreach (string key in m_NewDataRow.AllKeys)
            {
                if (!col.Contains(key))
                    col.Add(new System.Data.DataColumn(key));
            }

            var datarow = m_DataTable.NewRow();
            foreach (string key in m_NewDataRow.AllKeys)
            {
                datarow[key] = m_NewDataRow[key];
            }

            m_DataTable.Rows.Add(datarow);
            m_NewDataRow.Clear();

        }

        // Overload of its two parameter version
        public System.Data.DataView ToDataView()
        {
            return ToDataView(null, null);

        }

        /// <summary>
        /// Returns a Data view with subset of the data from its module variable of type Data table.
        /// </summary>
        /// <param name="filterExpression">name LIKE '%ACO%'</param>
        /// <param name="sortExpression">age DESC</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.Data.DataView ToDataView(string filterExpression, string sortExpression)
        {
            if (!HasRows)
            {
                filterExpression = null;
                sortExpression = null;
            }

            return new System.Data.DataView(m_DataTable
                , filterExpression
                , sortExpression
                , System.Data.DataViewRowState.CurrentRows);

        }

        // Overload of its two parameter version
        public System.Data.DataTable ToDataTable()
        {
            return ToDataTable(null, null);

        }

        /// <summary>
        /// Returns a Data table with subset of the data from its module variable of type Data table.
        /// </summary>
        /// <param name="filterExpression">size > 5</param>
        /// <param name="sortExpression">age DESC</param>
        /// <returns>Data table</returns>
        /// <remarks>Remember to dispose the DataTable obtained from this method.</remarks>
        public System.Data.DataTable ToDataTable(string filterExpression, string sortExpression)
        {
            return ToDataView(filterExpression, sortExpression).ToTable();

        }

        // Overload of its two parameter version
        public string ToHtmlTable()
        {
            return ToHtmlTable(null, null);

        }

        /// <summary>
        /// Returns a HTML table with subset of the data from its module variable of type Data table.
        /// </summary>
        /// <param name="filterExpression">size > 5</param>
        /// <param name="sortExpression">age DESC</param>
        /// <returns>HTML table</returns>
        /// <remarks></remarks>
        public string ToHtmlTable(string filterExpression, string sortExpression)
        {
            int i = 0;
            using (var dataTable = ToDataTable(filterExpression, sortExpression))
            {
                var sb = new System.Text.StringBuilder();
                sb.Append("<table>");
                sb.Append("<tr style=\"background-color: yellow;\">");
                foreach (System.Data.DataColumn col in dataTable.Columns)
                {
                    sb.Append("<td>");
                    sb.Append(col.ColumnName);
                    sb.Append("</td>");
                }

                sb.Append("</tr>");
                foreach (System.Data.DataRow row in dataTable.Rows)
                {
                    i += 1;
                    if (i % 2 == 0)
                    {
                        sb.Append("<tr style=\"background-color: WhiteSmoke;\">");
                    }
                    else
                    {
                        sb.Append("<tr>");
                    }

                    foreach (System.Data.DataColumn col in dataTable.Columns)
                    {
                        sb.Append("<td>");
                        sb.Append(row[col.ColumnName].ToString());
                        sb.Append("</td>");
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</table>");

                return sb.ToString();

            }

        }

        #region " IDisposable Support "

        // To detect redundant calls
        private bool disposedValue = false;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // free managed resources when explicitly called
                    m_DataTable.Dispose();
                }

            }
            this.disposedValue = true;

        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(true);
            System.GC.SuppressFinalize(this);

        }

        #endregion

    }

    public class AdhocDataExtension : AdhocData
    {

        public AdhocDataExtension()
            : base()
        {

        }

        public AdhocDataExtension(System.Data.DataTable dataTable)
            : base(dataTable)
        {

        }

        // Overload of its two parameter version
        public string ToCsv()
        {
            return ToCsv(null, null);

        }

        /// <summary>
        /// Returns a CSV file with subset of the data from its module variable of type Data table.
        /// </summary>
        /// <param name="filterExpression">size > 5</param>
        /// <param name="sortExpression">age DESC</param>
        /// <returns>CSV file</returns>
        /// <remarks></remarks>
        public string ToCsv(string filterExpression, string sortExpression)
        {
            char quotationMark = (char)34;
            char comma = (char)44;
            string delimiter = null;

            using (var dataTable = ToDataTable(filterExpression, sortExpression))
            {
                var sb = new System.Text.StringBuilder();
                delimiter = string.Empty;
                foreach (System.Data.DataColumn col in dataTable.Columns)
                {
                    sb.AppendFormat("{0}{1}{2}{1}"
                        , delimiter
                        , quotationMark
                        , col.ColumnName.Replace(quotationMark.ToString()
                        , quotationMark.ToString() + quotationMark.ToString())
                        , quotationMark.ToString());

                    delimiter = comma.ToString(); 
                }

                foreach (System.Data.DataRow row in dataTable.Rows)
                {
                    sb.AppendLine();
                    delimiter = string.Empty;
                    foreach (System.Data.DataColumn col in dataTable.Columns)
                    {
                        sb.AppendFormat("{0}{1}{2}{1}"
                            , delimiter
                            , quotationMark
                            , row[col.ColumnName].ToString().Replace(quotationMark.ToString()
                            , quotationMark.ToString() + quotationMark.ToString())
                            , quotationMark.ToString());

                        delimiter = comma.ToString();
                    }
                }

                return sb.ToString();

            }

        }

    }

}

