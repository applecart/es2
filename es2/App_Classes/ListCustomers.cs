﻿using System.Collections.Generic;
using System.Linq;

namespace es2.App_Classes
{

    public class CustomersViewModel
    {
        public string EditLink { get; set; }
        public int CustID { get; set; }
        public string CustName { get; set; }
        public string CustEmail { get; set; }
        public string PlanName { get; set; }
    }

    public class ListCustomers : AbstractClasses.ListEntityEfBase<CustomersViewModel>
    {
        protected override IEnumerable<CustomersViewModel> GetData(IQueryable<CustomersViewModel> entity)
        {
            foreach (var row in entity.ToList())
            {
                yield return new CustomersViewModel
                {
                    CustEmail = row.CustEmail,
                    CustID = row.CustID,
                    CustName = row.CustName,
                    PlanName = row.PlanName,
                    EditLink = GetEditLink("Edit", "Customers", row.CustID.ToString())

                };

            }
        }

        protected override IQueryable<CustomersViewModel> GetFilteredResults(DataTableAjaxPostModel model, IQueryable<CustomersViewModel> entity)
        {
            if (Filter == null)
            {
                return entity;

            }
            else
            {
                return entity.Where(e =>
                    e.CustID.ToString().Equals(Filter)
                    ||
                    (this.IsExactFilterOnly ? e.CustName.ToString().Equals(Filter) : e.CustName.ToString().Contains(Filter))
                    ||
                    (this.IsExactFilterOnly ? e.PlanName.ToString().Equals(Filter) : e.PlanName.ToString().Contains(Filter))
                    ||
                    (this.IsExactFilterOnly ? e.CustEmail.ToString().Equals(Filter) : e.CustEmail.ToString().Contains(Filter))
                    );

            }

        }

    } // class

}