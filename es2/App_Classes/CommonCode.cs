﻿

public static class CommonCode
{

    public static string GetVconfig(string key)
    {
        return System.Configuration.ConfigurationManager.AppSettings[key];

    }

    public static string GetAppUrl()
    {
        // This code is tested to work on all environments
        var oRequest = System.Web.HttpContext.Current.Request;
        return oRequest.Url.GetLeftPart(System.UriPartial.Authority)
            + System.Web.VirtualPathUtility.ToAbsolute("~/");

    }

    public static string UrlCombine(params string[] urls)
    {
        string retVal = string.Empty;
        foreach (string url in urls)
        {
            var path = url.Trim().TrimEnd('/').TrimStart('/').Trim();
            retVal = string.IsNullOrWhiteSpace(retVal) ? path : new System.Uri(new System.Uri(retVal + "/"), path).ToString();
        }
        return retVal;

    }

    public static string GetAppPath()
    {
        return System.Web.HttpContext.Current.Request.PhysicalApplicationPath;

    }

}
