﻿/*
 A class template to authorize Web API methods.
 Use this technique over SSL communication because the request header contains the credentials
   
*/


public class RestrictAttribute : System.Web.Http.AuthorizeAttribute
{
    protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
    {
        if (System.Threading.Thread.CurrentPrincipal.Identity.Name.Length == 0)
        { // If an identity has not already been established by other means:
            var auth = actionContext.Request.Headers.Authorization;
            if (auth.Scheme.Equals("Basic"))
            {
                var credentials = GetCredentials(auth.Parameter);
                if (IsAuthenticated(credentials[0], credentials[1])) { SetUser(credentials[0]); }

            }
        }
        return base.IsAuthorized(actionContext);
    }

    // Called from IsAuthorized()
    private string[] GetCredentials(string authParameters)
    {
        return System.Text.UTF8Encoding.UTF8.GetString(System.Convert.FromBase64String(authParameters)).Split(':');

    }

    // Called from IsAuthorized()
    private bool IsAuthenticated(string userName, string password)
    {
        // return System.Web.Security.Membership.ValidateUser(userName, password);
        return true; // your code for authentication

    }

    // Called from IsAuthorized()
    private void SetUser(string userId)
    {
        System.Security.Principal.GenericIdentity webIdentity = new System.Security.Principal.GenericIdentity(userId, "Form");
        System.Security.Principal.GenericPrincipal principal = new System.Security.Principal.GenericPrincipal(webIdentity, GetRoles(userId));
        System.Web.HttpContext.Current.User = principal;
        System.Threading.Thread.CurrentPrincipal = principal;

    }

    // Called from SetUser()
    private string[] GetRoles(string userId)
    {
        //return System.Web.Security.Roles.Provider.GetRolesForUser(userId);
        return new string[] { "Role 1", "Role 2", "Role 3" }; // your code to resolve all roles of the user

    }

} // class

