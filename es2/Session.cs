namespace es2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Session
    {
        [Key]
        [StringLength(80)]
        public string Key { get; set; }

        public string ValueBase64 { get; set; }

        public DateTime ExpireDtUtc { get; set; }
    }
}
