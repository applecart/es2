﻿
namespace es2
{
    public partial class UnitTest : System.Web.UI.Page
    {
        protected void Button1_Click(object sender, System.EventArgs e)
        {
            this.Literal1.Text = CreateAndPopulateTables();

        }

        private string CreateAndPopulateTables()
        {
            using (var rs = new Framework_Code.TxnWriteSqlServer("ES"))
            {
                //CreateTables(rs);
                //// Generate a random csv on b:\temp\part.csv of 100 records with CustName, CustEmail columns from http://convertcsv.com/generate-test-data.htm
                //rs.ExecuteBulkCopy("dbo.Customers", CreateCustomers(), GetCustomerColumnMap());
                //rs.ExecuteBulkCopy("dbo.Plans", CreatePlans(), GetPlanColumnMap());
                //rs.Commit();
                return "Done!";

            }

        }

        private void CreateTables(Framework_Code.TxnWriteSqlServer rs)
        {
            var sql = @"
                CREATE TABLE [dbo].[Customers](
	                [CustID] [int] IDENTITY(1,1) NOT NULL,
	                [CustName] [varchar](50) NULL,
	                [CustEmail] [varchar](50) NULL,
	                [PlanID] [int] NULL,
                 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
                (
	                [CustID] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]

                 CREATE TABLE [dbo].[Plans](
	                [PlanID] [int] IDENTITY(1,1) NOT NULL,
	                [PlanName] [varchar](50) NULL,
	                [PlanFixedCost] [decimal](8, 4) NULL,
	                [PlanVarCost] [decimal](8, 4) NULL,
                 CONSTRAINT [PK_Plans] PRIMARY KEY CLUSTERED 
                (
	                [PlanID] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY] 

                ";

            rs.ExecuteNonQuery(sql); 

        }

        private System.Data.DataTable CreateCustomers()
        {
            var dt = CsvToDataTable(System.IO.Path.Combine(CommonCode.GetAppPath(),"App_Data","customers.csv"));
            dt.AddColumn("PlanID", null);
            var rnd = new System.Random();
            foreach (System.Data.DataRow row in dt.Rows)
            {
                row["PlanID"] = rnd.Next(1, 11);
            }

            return dt;

        }

        private System.Data.DataTable CreatePlans()
        {
            var adHoc = new Framework_Code.AdhocData();
            var rnd = new System.Random ();
            foreach (var plan in "Alpha,Bravo,Charlie,Delta,Echo,Fox,Golf,Hotel,India,Juliet,Kilo".Split(','))
            {
                adHoc.SetColumnValue("PlanName", plan + " Plan");
                adHoc.SetColumnValue("PlanFixedCost", System.Math.Round(rnd.Next(0, 5) + rnd.NextDouble(), 4).ToString());
                adHoc.SetColumnValue("PlanVarCost", System.Math.Round(rnd.Next(5, 10) + rnd.NextDouble(), 4).ToString());
                adHoc.AddRow();
            }

            return adHoc.ToDataTable();

        }
        private System.Collections.Generic.Dictionary<string, string> GetCustomerColumnMap()
        {
            var columnsMap = new System.Collections.Generic.Dictionary<string, string>();
            // The destination columns are on the left so source columns may be re-used.
            columnsMap.Add("[CustName]", "CustName");
            columnsMap.Add("[CustEmail]", "CustEmail");
            columnsMap.Add("[PlanID]", "PlanID");

            return columnsMap;
        }

        private System.Collections.Generic.Dictionary<string, string> GetPlanColumnMap()
        {
            var columnsMap = new System.Collections.Generic.Dictionary<string, string>();
            // The destination columns are on the left so source columns may be re-used.
            columnsMap.Add("[PlanName]", "PlanName");
            columnsMap.Add("[PlanFixedCost]", "PlanFixedCost");
            columnsMap.Add("[PlanVarCost]", "PlanVarCost");

            return columnsMap;

        }

        private System.Data.DataTable CsvToDataTable(string fileName)
        {
            return CsvToDataTable(fileName, string.Format("SELECT * FROM [{0}]", System.IO.Path.GetFileName(fileName)));

        }

        private System.Data.DataTable CsvToDataTable(string fileName, string sql)
        {
            using (var rs = new Framework_Code.DataAccess("System.Data.Odbc", "Driver={Microsoft Text Driver (*.txt; *.csv)};DBQ=[DirectoryName];".Replace("DirectoryName", System.IO.Path.GetDirectoryName(fileName))))
            {
                return rs.ExecuteQuery(sql);

            }

        }

    } // class

}