namespace es2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Plan
    {
        public int PlanID { get; set; }

        [StringLength(50)]
        public string PlanName { get; set; }

        public decimal? PlanFixedCost { get; set; }

        public decimal? PlanVarCost { get; set; }
    }
}
