namespace es2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("name=ES")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Plan>()
                .Property(e => e.PlanFixedCost)
                .HasPrecision(11, 2);

            modelBuilder.Entity<Plan>()
                .Property(e => e.PlanVarCost)
                .HasPrecision(11, 2);

            modelBuilder.Entity<Session>()
                .Property(e => e.Key)
                .IsUnicode(false);

            modelBuilder.Entity<Session>()
                .Property(e => e.ValueBase64)
                .IsUnicode(false);
        }
    }
}
